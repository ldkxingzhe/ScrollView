package ldk.util.scrollview;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.FrameLayout;

/**
 * Created by ldkxingzhe@163.com on 2017/9/10.
 */

public class TestScrollBar extends Activity{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_scrollbar);
        final FrameLayout frameLayout = (FrameLayout) findViewById(R.id.fl_frame);
        findViewById(R.id.btn_action).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                frameLayout.scrollBy(0, 50);
            }
        });
    }
}
