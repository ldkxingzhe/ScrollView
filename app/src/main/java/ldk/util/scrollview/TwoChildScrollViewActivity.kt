package ldk.util.scrollview

import android.app.Activity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_two_child.*

/**
 * Created by ldkxingzhe@163.com on 2017/9/9.
 */
class TwoChildScrollViewActivity : Activity(){

    private var fistTime = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_two_child)
        btn_action.setOnClickListener {
            if (fistTime){
                web_view.loadUrl("http://m.sohu.com/n/394168771/")
                /*web_view.loadData("<!doctype html><html><head><meta charset=\"utf-8\"/><meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"/><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, user-scalable=0, maximum-scale=1, minimum-scale=1\"/><title></title><style>*{margin: 0;padding: 0;color: #777;font-weight: normal;font-size: 1.3rem;font-family: PingFangSC-Regular;}body{margin: 0;padding-left: 0.3rem;padding-right: 0.3rem;}p{width:100%;margin-top: 0;margin-bottom: 0.3rem;overflow-wrap: break-word;}img{width: 100%!important;height: auto;margin: 0.3rem 0;}</style></head><body><p></p>\n" +
                        "<img src=\"http://rjtx-dev.oss-cn-beijing.aliyuncs.com/test0/3472efc578422e08e9399602b5c0edee.png\" alt=\"undefined\" style=\"float:none;height: auto;width: auto\"/>\n" +
                        "<p>fafj房间爱书法家发神经了房间法拉盛放假啦十分激烈的三分副书记奥拉夫经发局收代理费fas房间爱老费劲啊酸辣粉发酵饲料房间爱死了发就发生发神经六块腹肌阿萨德路附近撒劳动法副书记阿斯利康房间爱睡懒觉法拉盛框架法拉盛地方近房间爱上了房间爱上了放假啊当上了飞机放假阿斯兰的咖啡机奥斯卡了房间里卡圣诞节福利卡设计费拉屎解放路卡萨丁防腐剂奥斯卡了房间拉屎积分辣的设计费放假啊删掉了放假啊当上了飞机</p>\n" +
                        "<p>放假阿里速度快放假啦司法局拉风放假阿里上课放假啦束带结发时代峻峰垃圾啊收到款法师里房间爱上了房间卢卡斯解放啦世纪东方发生的纠纷拉丝机法拉盛京东方卡拉圣诞节发大水了房间里</p>\n" +
                        "</body></html>\n", "text/html; character=utf8", null);*/
                fistTime = false
            }else{
                web_view.scrollBy(0, 3000)
            }
        }
        last_line.setOnClickListener {
            Log.d("TwoChild", "点击了最后一行")
        }
    }
}