package ldk.util.scrollview

import android.content.Context
import android.util.AttributeSet
import android.webkit.WebView

/**
 * Created by ldkxingzhe@163.com on 2017/9/6.
 */
class WebViewLongest(context: Context?, attrs: AttributeSet?) : WebView(context, attrs) {

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, MeasureSpec.makeMeasureSpec((Int.MAX_VALUE shr  2), MeasureSpec.AT_MOST))
    }
}