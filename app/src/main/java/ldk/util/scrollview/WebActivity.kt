package ldk.util.scrollview

import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.webkit.WebView
import android.webkit.WebViewClient
import kotlinx.android.synthetic.main.activity_web.*
import ldk.util.scrollviews.afterFinished
import ldk.util.scrollviews.setOnImageClick

/**
 * Created by ldkxingzhe@163.com on 2017/9/6.
 */
class WebActivity: Activity() {
    companion object {
        private var TAG = "WebActivity";
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web)
        web_view.setOnImageClick { list, i ->
            Log.v(TAG, "onImageClick, and list is $list, index is $i")
        }
        web_view.setWebViewClient(object : WebViewClient() {
            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                Log.v(TAG, "onPageFinished")
                web_view.afterFinished()
            }
        })
        action.setOnClickListener {
            web_view.loadUrl("http://blog.csdn.net/qiyei2009/article/details/52708919")
        }
    }
}