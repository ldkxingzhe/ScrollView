package ldk.util.scrollview

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v4.widget.NestedScrollView
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : Activity() {
    companion object {
        private val TAG = "MainActivity";
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        web_activity.setOnClickListener {
            startActivity(Intent(this, WebActivity::class.java))
        }

        dialog.setOnClickListener {
            val build = AlertDialog.Builder(this)
            build.setMessage("测试")
            build.setNegativeButton("取消", null)
            build.setPositiveButton("确定", null)
            val dialog = build.show()
            dialog.setOnDismissListener {
                Log.v(TAG, "dialog dismissed")
            }
        }

        scroll_activity.setOnClickListener {
            startActivity(Intent(this, TwoChildScrollViewActivity::class.java))
        }

        btn_test_scroll_bar.setOnClickListener{
            startActivity(Intent(this, TestScrollBar::class.java))
        }
    }

    override fun onPause() {
        super.onPause()
        Log.v(TAG, "onPause")
    }
}
